var express = require('express');
var router = express.Router();

var lookup = require('../helpers/lookup');
var employees = require('../models/employees')



/* GET home page. */
router.get('/', function(req, res, next) {
  
  res.render('index', { title: 'Node Average Salary API' });
  
});


/* GET average salary of employee from title */
router.get('/:title', function(req, res, next) {

  var averageSalary = lookup.getAverageSalary( req.params.title );
  var status = !!averageSalary 
  				 ? 200
  				 : 400
  				 
  res.send( status, averageSalary );

});


module.exports = router;
