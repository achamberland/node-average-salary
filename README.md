# Boston Employee Average Salary API

Get average salary by title from the Boston Employee Earnings Report.

While data for individuals is given by this report, there isn't any information about earnings based on employee title. This API provides the average earnings of Boston city employees by title.


## Installation

Clone this repository, install its dependancies, and start the Node server:
```
git clone https://achamberland@bitbucket.org/achamberland/node-average-salary.git
cd node-average-salary
npm install
node start
```

This service will be running on http://localhost:3000.


## Usage

To make a request with curl, run `curl http://localhost:3000/[EMPLOYEE TITLE GOES HERE]`.

Because all money amounts in the Boston Employee Earnings Report are strings, this returns a string as well.


## Limitations

Because of the time constraints given, testing for this is basic- for example, the Express server responses aren't being tested. I wanted to add a UI accessible from http://localhost:3000 which would let users select from a list of available titles and filter them. Also, if I could do this project over again, I would've added an `/api/` path after the base url to separate API requests from non-API requests.


## Projects

My other projects can be found at <https://github.com/achamberland>.

#### CSS Minifier for Gulp
My current Node.js project is a new type of CSS minifier. Unlike other minifiers which only strip out whitespace, this minifies the selectors of your CSS, leading to a 15-30% smaller file from some of my tests with CSS files at Gazelle.

In the future I'm planning on more experimental features like javascript/jQuery selector replacement, id replacement, and property minification. For now the project is still in early stages.

#### Other projects
- A website template written in React that gives websites a 3D effect when navigating to different views, using CSS3 animations. <http://achamberland.com>.
- A React Native chat app for iOS and Android, in early development.

Finally, here's a link to my resume. 
<https://drive.google.com/file/d/0BxTTwo9Jxv3kUFVLSTlXeHRkQkk/view?usp=sharing>