var employees = require('../models/employees.json')

module.exports = {

	getEmployeesByProperty: function ( queryKey, queryVal ) {
    console.log(queryKey); console.log( queryVal);
		queryVal = decodeURIComponent(queryVal);

		return employees.filter( (listing) => {
	  	if (listing[queryKey] === queryVal) return true;
	  });

	},

  getAverageSalary: function( title ) {

    var employees = this.getEmployeesByProperty( 'title', title );
    var salarySum = 0;

    employees.forEach( function(employee) {
      salarySum += parseFloat( employee.total_earnings );
    })

    var average = (salarySum / employees.length).toFixed(2);

    return average ;

  }

};
