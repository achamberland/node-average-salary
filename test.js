var expect = require("chai").expect;

var lookup = require('./helpers/lookup');

describe("Average Salary API", function() {

  describe("Lookup employees with title", function() {

    it("Returns correct total_earnings from the first employee with certain title", function(done) {

      var employee = lookup.getEmployeesByProperty( "title", encodeURIComponent("Head Lifeguard") );

      var firstLifeguard = {
        "total_earnings" : "6556.85",
        "zip" : "02118",
        "detail" : "0.00",
        "injured" : "0.00",
        "title" : "Head Lifeguard",
        "other" : "0.00",
        "regular" : "6428.73",
        "name" : "Benson,Maurice",
        "retro" : "0.00",
        "department_name" : "Boston Cntr - Youth & Families",
        "overtime" : "128.12",
        "quinn" : "0.00"
      };

      expect( employee[0].total_earnings ).to.equal( firstLifeguard.total_earnings )

      done();

    });

    it("Calculates average salary from a set of employee data by title", function(done) {

      var result = lookup.getAverageSalary( encodeURIComponent('Head Lifeguard') );

      expect(result).to.equal("31033.21");

    	done();

    });

  });

});